import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import React, {Component} from 'react';

class ClassButton extends Component {
  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.button}>
        <Text style={styles.buttonLabel}>{this.props.label}</Text>
      </TouchableOpacity>
    );
  }
}

export default ClassButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'pink',
    width: '100%',
    alignItems: 'center',
    borderRadius: 20,
    padding: 10,
  },
  buttonLabel: {
    color: 'white',
    fontSize: 16,
    fontWeight: '800',
  },
});
