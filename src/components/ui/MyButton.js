import React from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import PropTypes from 'prop-types';

const MyButton = props => {
  return (
    <TouchableOpacity style={styles.button} onPress={props.onPress}>
      <Text style={styles.buttonLabel}>{props.label}</Text>
    </TouchableOpacity>
  );
};

MyButton.propTypes = {
  label: PropTypes.string,
  onPress: PropTypes.func,
};

MyButton.defaultProps = {
  label: 'Add a title',
  onPress: () => {},
};

export default MyButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: 'purple',
    width: '100%',
    alignItems: 'center',
    borderRadius: 20,
    padding: 10,
  },
  buttonLabel: {
    color: 'white',
    fontSize: 16,
    fontWeight: '800',
  },
});
