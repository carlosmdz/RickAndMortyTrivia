import React from 'react';
import {View, StyleSheet, Text, Button} from 'react-native';

// [Home, Details, Details, Details, Details, Details, Details]

const Winner = ({route, navigation}) => {
  const {jugador} = route.params;
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Ganador!!</Text>
      <Text
        style={styles.text}>{`Te felicito ${jugador} que bien actuas.`}</Text>
      <Button title="Jugar de nuevo" onPress={() => navigation.popToTop()} />
    </View>
  );
};

export default Winner;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 15,
  },
  title: {
    backgroundColor: 'white',
    fontSize: 20,
    color: 'green',
    marginBottom: 10,
  },
  text: {color: 'black'},
});
