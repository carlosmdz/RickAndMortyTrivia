import React from 'react';
import {View, StyleSheet, Text, Button} from 'react-native';

// [Home, Details, Details, Details, Details, Details, Details]

const Details = ({route, navigation}) => {
  const {animal, meses} = route.params;
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Details</Text>
      <Text
        style={styles.text}>{`Mi ${animal} tiene ${meses} meses conmigo`}</Text>
      <Button
        title="Go to Details Again :o"
        onPress={() => navigation.push('Details')}
      />
      <Button title="Ir hasta arriba" onPress={() => navigation.popToTop()} />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
};

export default Details;

const styles = StyleSheet.create({
  container: {},
  text: {color: 'black', alignSelf: 'center'},
});
