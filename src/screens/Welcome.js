import React, {useState} from 'react';
import {View, StyleSheet, Text, TextInput} from 'react-native';
import ClassButton from '../components/ui/ClassButton';
import MyButton from '../components/ui/MyButton';

const Welcome = props => {
  const {navigation} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Login</Text>
      <View>
        <TextInput
          style={styles.input}
          onChangeText={setEmail}
          autoCapitalize="none"
          keyboardType="email-address"
          placeholder="Email"
          value={email}
        />
        <TextInput
          style={styles.input}
          onChangeText={setPassword}
          autoCapitalize="none"
          keyboardType="email-address"
          placeholder="Password"
          value={password}
        />
      </View>
      <View style={styles.footer}>
        <MyButton onPress={() => console.log('Hola desde el componente 😍')} />
        <ClassButton
          label="Class button"
          onPress={() => console.log('Hola desde la clase')}
        />
      </View>
    </View>
  );
};

export default Welcome;

const styles = StyleSheet.create({
  container: {padding: 15},
  text: {color: 'black', fontSize: 30, alignSelf: 'center', marginBottom: 40},
  input: {
    borderBottomColor: 'gray',
    borderBottomWidth: 0.5,
    paddingBottom: 2,
    marginVertical: 20,
  },
  button: {
    // alignSelf: 'center',
    backgroundColor: 'purple',
    width: '100%',
    alignItems: 'center',
    borderRadius: 20,
    padding: 10,
  },
  buttonLabel: {
    color: 'white',
    fontSize: 16,
    fontWeight: '800',
  },
  footer: {
    marginVertical: 20,
  },
});
