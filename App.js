import React from 'react';

// Agregamos librerías de React Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// Pantallas de la aplicación
import Welcome from './src/screens/Welcome';
import Details from './src/screens/Details';
import Winner from './src/screens/Winner';

// Creamos función para agregar y agrupar pantallas
const Stack = createNativeStackNavigator();

// NavigationContainer envuelve nuestra aplicación para que podamos usar la navegación
// entre pantallas, y ayuda a que la app sea responsiva.
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Welcome">
        {/* Aquí se agregan las pantallas */}
        <Stack.Screen name="Welcome" component={Welcome} />
        <Stack.Screen name="Details" component={Details} />
        <Stack.Screen name="Winner" component={Winner} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
